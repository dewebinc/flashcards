function flipCard(){
	var card = document.getElementById("flip-container");	
	var front = document.getElementById("front");
	
	card.classList.toggle("clicked");
	front.style.visibility = "visible"
	
}

function newCard() {
	var card = document.getElementById("flip-container");
	var front = document.getElementById("front");
	var back = document.getElementById("back");
	var randNum = Math.floor((Math.random() * 10) + 1);
	var randNum2 = Math.floor((Math.random() * 10) + 1);
	var sum = randNum * randNum2;

	card.classList.toggle("clicked");
	back.innerHTML = randNum + " X " + randNum2;
	front.style.visibility = "hidden";
	front.innerHTML = sum;
}